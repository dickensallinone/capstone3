import { useReducer,useEffect} from "react";
import AppContext from "./app-context";
import {addToCartHandler} from "../util/retainCartItems";

const defaultCartState ={
    items:[],
    totalAmount: 0,
    changed: false
  }
  
  const cartReducer = (state,action)=>{
    if(action.type === 'ADD'){
      const updatedTotalAmount = state.totalAmount + action.item.price * action.item.quantity;
      const duplicateOrderIndex = state.items.findIndex(val=>val.id === action.item.id);
      const existingOrder = state.items[duplicateOrderIndex];
     
      let updatedArr;
      state.changed = true;
      if(existingOrder){
        const updatedItem = {
          ...existingOrder,
          quantity: existingOrder?.quantity+ action.item.quantity,
          price: existingOrder?.price
        }
        updatedArr=[...state.items];
        updatedArr[duplicateOrderIndex] = updatedItem;
  
      } else{
        updatedArr = state.items.concat(action.item);
      }   
      return{
        items: updatedArr,
        totalAmount: updatedTotalAmount
      }
    }

    if(action.type === 'REMOVE') {
      const findTargetItem = state.items.findIndex(val=>val.id === action.id);
      const existingOrder = state.items[findTargetItem];
      
       const updatedArrTargetItem = {
          ...existingOrder,
          quantity: existingOrder.quantity-1,
          
        }

      let updatedArr;
      let updatedTotalAmount;
      state.changed = true;
      updatedTotalAmount = state.totalAmount - existingOrder?.price;
      if (updatedArrTargetItem.quantity >0)  {
          
          updatedArr = [...state.items]; 
          updatedArr[findTargetItem] = updatedArrTargetItem;
          
      }
      else {
      
        updatedArr = [...state.items].filter(val=>val.id !== existingOrder.id);
        
      } 
      return{
        items: updatedArr,
        totalAmount: updatedTotalAmount
      }
    }

    if(action.type === "RETAIN"){
      state.changed = false;

      return {
        items: action.item.items,
        totalAmount: action.item.totalAmount
      }
    }
   
    if(action.type === 'REMOVEALL') return defaultCartState;
 
  }

  const defaultLoginState = {
    id: null,
    isAdmin: null
  }

  const loginReducer = (state,action)=>{
    if(action.type === 'LOGIN_VALID'){
   
        return {
            id: action.id,
            isAdmin: action.isAdmin,
    
        }
    }
    if(action.type === 'LOGIN_INVALID') {
      
        return defaultLoginState;
    }

    return defaultLoginState;
  }

const AppProvider =(props)=>{    
    const [cartState,dispatchCartState] = useReducer(cartReducer,defaultCartState);
    const [loginState,dispatchLoginState] = useReducer(loginReducer,defaultLoginState);

    function addItem(item) {
     dispatchCartState({type:'ADD',item:item});

    }
    function removeItem(id) {
     dispatchCartState({type:'REMOVE',id:id})
    }

    function retainItems(item) {
      dispatchCartState({type:'RETAIN',item:item})
    }
   
    function removeAllItems() {
     dispatchCartState({type:'REMOVEALL'})
    }

    function validLogin({id,isAdmin}) {
        dispatchLoginState({type: 'LOGIN_VALID',id:id,isAdmin:isAdmin})
    }

    function invalidLogin() {
        dispatchLoginState({type: 'LOGIN_INVALID'})
    }

    useEffect(()=>{

      if(!cartState.changed){
        return;
      }else {
        addToCartHandler();
      }
      async function addToCartHandler(){
        const updateCart = await fetch(`${process.env.REACT_APP_API_URL}/users/addtocart`,{
          method: "PUT",
          headers:{
          'Content-Type': 'application/json',
          Authorization: `Bearer ${localStorage.getItem("token")}`},
          body: JSON.stringify({
            items: cartState.items,
            totalAmount: cartState.totalAmount
          })
        }) 
  
        if(!updateCart.ok){
          return "something went wrong"
        }

        const result = await updateCart.json();
        return result;
      }
    },[cartState])

   async function obtainTokenUponLogin(val){
    const data = await fetch(`${process.env.REACT_APP_API_URL}/users/profile`,{
      headers:{
          Authorization: `Bearer ${val}` 
      }
    })

    if(!data.ok) {
      return;
    }

    const dataShow = await data.json();


    const id = dataShow[0]?._id;
    const isAdmin  = dataShow[0]?.isAdmin;
    
    validLogin({id,isAdmin})
   }

    useEffect(()=>{
      async function fetchAnother(){
        const fetchedData =  await addToCartHandler();
        retainItems({items:fetchedData.items || [],
         totalAmount:fetchedData.totalAmount || 0});


         const data = await fetch(`${process.env.REACT_APP_API_URL}/users/profile`,{
          headers:{
              Authorization: `Bearer ${localStorage.getItem("token")}` 
          }
        })
    
        if(!data.ok) {
          return;
        }
    
        const dataShow = await data.json();
     
        const id = dataShow[0]?._id;
        const isAdmin  = dataShow[0]?.isAdmin;

        validLogin({id,isAdmin})
       } 
       fetchAnother();

       
    },[])

    
    
    const cartContext = {
        user: {id: loginState.id,isAdmin:loginState.isAdmin},
        cartOrders:cartState.items,
        totalAmount: cartState.totalAmount,
        addItem:addItem,
        removeItem:removeItem,
        removeAllItems:removeAllItems,
        validLogin:validLogin,
        invalidLogin:invalidLogin,
        retainItems:retainItems,
        obtainTokenUponLogin:obtainTokenUponLogin
    }
    return (
        <AppContext.Provider value={cartContext}>
            {props.children}
        </AppContext.Provider>
    )
}

export default AppProvider;