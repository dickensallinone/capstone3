import React from "react";

const AppContext=React.createContext({
    user: {},
    cartOrders:[],
    totalAmount:0,
    addItem:()=>{},
    removeItem:()=>{},
    removeAllItems:()=>{},  
    retainItems: ()=>{},
    validLogin: ()=>{},
    invalidLogin: ()=>{},
    clearingToken: ()=>{},
    addToCartHandler: ()=>{},
    obtainTokenUponLogin:()=>{}
})

export default AppContext;