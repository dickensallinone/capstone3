import React from "react";
import { Row } from "react-bootstrap";
import ProductItem from "./ProductItem";

export default function UserView(data) {
    const {productData} = data;
    const productDetails = productData?.map(val=>{
        return (
            val.isActive && <ProductItem key={val._id} productSpecific={val}/>
        )
    })

    return(
        <Row className="mt-3">
            {productDetails}
        </Row>
    )
}