import React,{Row,Col,Button} from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlusCircle} from '@fortawesome/free-solid-svg-icons';
const plusElement = <FontAwesomeIcon icon={faPlusCircle} />;

const AdminHeader =()=>{
    return(
        <React.Fragment>
             <Row className="d-flex align-items-center text-center">
                    <Col lg={9}><h1>Product Inventory</h1></Col>
                    <Col lg={3}><Button variant="secondary">{plusElement} Product</Button></Col>
            </Row>

            <Row>
                <Col lg={4}>Total Items</Col>
                <Col lg={4}>Active Items</Col>
                <Col lg={4}>Not Active Items</Col>
            </Row>
        </React.Fragment>
    )
}

export default AdminHeader;