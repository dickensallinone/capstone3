import React from "react";
import { Button, Form } from "react-bootstrap";
import { json ,useRouteLoaderData} from "react-router-dom";
import { useForm } from "react-hook-form";

export default function UpdateCredentials({toggleUpdateCredForm}){
      const { register, handleSubmit, formState:{errors} } = useForm();
      const token = useRouteLoaderData("root");
      const onSubmit = async(data) => {
        const {email,password} = data;

        const result = await fetch(`${process.env.REACT_APP_API_URL}/users/updatecredentials`,{
          method: "POST",
          headers: {
              'Content-Type': 'application/json',
              Authorization: `Bearer ${token}`
          },
          body: JSON.stringify({email,password})
      })

      if(!result.ok) {
        throw json({message: "Something went wrong"},
        {status: 500})
    }

      const resultData = await result.json();

      console.log(resultData);
      }
       
      
      return(
      <Form onSubmit={handleSubmit(onSubmit)}>
        <Form.Group className="mb-3" controlId="new-email">
          <Form.Label>Email address</Form.Label>
          <Form.Control type="email" placeholder="Your New Email Here" name="email" 
            {...register("email", {required: true, pattern: /^\S+@\S+$/i})} 
          />
          {errors.email && <p>Email is required and must be valid</p>}
        </Form.Group>

       <Form.Group className="mb-3" controlId="new-password">
          <Form.Label>Password</Form.Label>
          <Form.Control type="password" placeholder="Your New Password Here" name="password"
             {...register("password", {required: true,minLength: 10, maxLength: 40})} 
          />
          {errors.password && <p>Password is required</p>}
        </Form.Group>

        <Form.Group>
          <Button type="button" variant="secondary" onClick={toggleUpdateCredForm}>
            Cancel
          </Button>

          <Button type="submit" variant="primary">
            Save
          </Button>
        </Form.Group>
      </Form>
      );
}