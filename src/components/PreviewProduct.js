import { Col,Card} from "react-bootstrap";
import { Link } from "react-router-dom";

export default function PreviewProduct(props){
    const {breakPoint,data,keyValue} = props;
    const {
        _id,name,description,price
    } = data;
    return(
        <Col lg={breakPoint} md={breakPoint +2} sm={breakPoint *2 +2} key={keyValue}>
            <Card className="cardHighlight mx-auto">
                 
                    <Card.Body>
                    
                        <Card.Title className="text-center">
                        {name}
                        </Card.Title>
                        <Card.Text>
                            {description}
                        </Card.Text>
                    </Card.Body>

                    <Card.Footer>
                        <h5 className="text-center">Php {price}</h5>
                        <Link className="btn btn-primary d-block" to={`/products/${_id}`}>Details</Link>
                    </Card.Footer>
            </Card>
        </Col>
    )
}