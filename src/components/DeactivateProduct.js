import { Button} from "react-bootstrap";
import Swal from 'sweetalert2';
import useHttp from "../hooks/use-http";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPowerOff} from '@fortawesome/free-solid-svg-icons';
const activateElement = <FontAwesomeIcon icon={faPowerOff} />;

export default function DeactivateProduct({product,productActive,getAllProducts,retriveData}){
    const {sentRequest:updateProductStatus} = useHttp();
    function reusableFunction(isActiveValue){
        const routeDef = productActive ? 'archiveproduct': 'activateproduct';

        updateProductStatus({url: `${process.env.REACT_APP_API_URL}/products/${product}/${routeDef}`,
        method: "PUT",
        headers:{
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
        },
        body: {
            isActive: isActiveValue
        }},getUpdateResultHandler)
    }

    function getUpdateResultHandler(data){
        if(data) {
            Swal.fire({
                title: `Product is ${productActive ? 'Archived': 'Activated'} Successfully`,
                icon: 'success',
                text: 'Course Successfully Updated'
               })
            getAllProducts({url: `${process.env.REACT_APP_API_URL}/products/allproducts`},retriveData);
         }
         else{
            Swal.fire({
                title: 'Update failed',
                icon: 'error',
                text: 'Please try again!'
                })
         }
       
        
    }
    function archiveToggle(){
        reusableFunction(false)
    }

    function activateToggle(){
        reusableFunction(true)
    }

    return(

        <Button variant={productActive? "danger":"success"}
             onClick={productActive ? archiveToggle : activateToggle}
            >{
                activateElement
            }</Button>
    )
}