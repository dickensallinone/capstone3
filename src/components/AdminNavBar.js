import { Nav } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import ClientNavbarDesign from "../reusables/ClientNavbarDesign";
export default function AdminNavBar(){
    return(
        <ClientNavbarDesign>
            <Nav variant="pills" className="me-auto client-nav" >

                <Nav.Item>
                    <Nav.Link as={NavLink} to={'/products'} end>Manage Products</Nav.Link>
                </Nav.Item>            
                <Nav.Item>
                    <Nav.Link as={NavLink} to={'users-orders'}>Orders</Nav.Link>
                </Nav.Item>

                <Nav.Item>
                    <Nav.Link as={NavLink} to={'users'}>Users</Nav.Link>
                </Nav.Item>
                
                {/* <Nav.Item>
                    <Nav.Link as={NavLink} to={'activities'}>Activities</Nav.Link>
                </Nav.Item>
                 */}
            </Nav>
        </ClientNavbarDesign>
        )
}