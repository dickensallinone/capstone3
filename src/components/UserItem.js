import React from "react"
import { Col } from "react-bootstrap"
export default function UserItem({id,firstName,lastName}){

    return (
        <React.Fragment>
            <Col key={id}>
                <p>{firstName}</p>
                <p>{lastName}</p>
            </Col>
        </React.Fragment>
    )
}