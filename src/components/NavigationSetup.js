import React, { useContext } from "react";
import UserNavBar from "./UserNavBar";
import AdminNavBar from "./AdminNavBar";
import AppContext from "../store/app-context";
import { Outlet, useNavigation} from "react-router-dom";
import { Spinner } from "react-bootstrap";
export default function NavigationSetup (){
    const token = localStorage.getItem("token");
    const {user} = useContext(AppContext);
    const navigate = useNavigation();
    const useNav = user?.isAdmin?  <AdminNavBar/> : <UserNavBar/>;

    return (
        <React.Fragment>
         {navigate.state === 'loading'&&
         <Spinner animation="border" role="status">
        </Spinner>}
        {token && useNav}
             <Outlet/>
        </React.Fragment>
       
    )
}