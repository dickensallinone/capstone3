import { useState,useEffect} from "react";
import { Row, Container} from "react-bootstrap";
import PreviewProduct from "./PreviewProduct";

import '../index.scss';

export default function Featured({data}){
    const [previews,setPreviews]= useState([]);
    
 

    useEffect(()=>{
        const numbers = [];
        const featured = [];
        const generateRandomNums = ()=>{
            let randomNum = Math.floor(Math.random() * data?.length);
            if(numbers.indexOf(randomNum) === -1){
                numbers.push(randomNum);
             }else{
                generateRandomNums();
             }
        }
    
        for (let i = 0; i < 3; i++) {
            generateRandomNums();

            featured.push(
                <PreviewProduct data={data[numbers[i]]}
                    keyValue={data[i]?._id}
                    breakPoint={4}
                    key={data[i]?._id}
                />
            )
    
            setPreviews(featured)
        }

       
    },[data])
  
    return(
        <Container className="featured">
            <h2 className="text-center" >
                Featured Products
            </h2>

            <Row xs={1} md={2} className="g-4" >
            {previews}
            </Row>
        </Container>
    )
}