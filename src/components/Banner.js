import {Row, Col} from 'react-bootstrap';
import '../index.scss';
import { Link } from 'react-router-dom';

export default function Banner(props){
	const {header,content, refRoute,buttonLabel} = props;
	return(
		<Row>
			<Col className="p-5 text-center banner">
				<h1>{header}</h1>
				<p>{content}</p>
				<Link to={refRoute} relative=''
				variant="primary">{buttonLabel}</Link>
				</Col>
		</Row>
	)
}