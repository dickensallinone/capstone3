import React, { } from "react";
import { ListGroup,ListGroupItem } from "react-bootstrap";
export default function OrderedProductsList({list,keyValue}){
    return (
        <ListGroup key={keyValue} as="ol" numbered>
            {list.map(value=>(
                <ListGroupItem key={value.productId} as="li">
                    {value.productName} ({value.quantity})
                </ListGroupItem>
            ))}

        </ListGroup>
    )
}