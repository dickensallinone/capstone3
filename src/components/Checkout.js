import { useContext,useEffect,useState} from "react";
import { Offcanvas,Button } from "react-bootstrap";
import AppContext from '../store/app-context';
import useHttp from "../hooks/use-http";
import { Link, useNavigate } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMinus,faPlus } from "@fortawesome/free-solid-svg-icons";
const minusIcon = <FontAwesomeIcon icon={faMinus}/>;
const plusIcon = <FontAwesomeIcon icon={faPlus}/>;

export default function Checkout({offcanvasControl,show,
  deliveryinfoHandler,
  deliveryInfoExist}){
    const {cartOrders,totalAmount,removeAllItems,removeItem,addItem} = useContext(AppContext);
    const navigate = useNavigate();

    const [checkoutResults,setCheckoutResults] = useState({
      isSuccessful: null,
      message: null
      })

      const {sentRequest: updateItemsInCartFunc} = useHttp();
     
      useEffect(()=>{
        updateItemsInCartFunc({url: `${process.env.REACT_APP_API_URL}/users/addtocart`,
        method: "PUT",
        headers:{
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
        },
        body: {
            items: cartOrders,
            totalAmount: totalAmount
        }},updatingItemsInCart)
    },[cartOrders,totalAmount,updateItemsInCartFunc])

    function updatingItemsInCart(data){
      console.log(data);
    }
    
    const showOrder = cartOrders?.map(val=>
        {
            return(
               <div key={val.id}>
  
                 <div >
                    <p >{val.name}</p>
                    <p>{val.price}</p>
                    <p>{val.quantity}</p>
                </div>

                <div >
                  <Button variant="dark" onClick={()=>{
                     
                    removeItem(val.id);
                   
                  }}>
                    {minusIcon}
                  </Button>

                  <Button variant="dark" onClick={()=>{
                    
                    addItem({
                    id:val.id,
                    name:val.name,
                    description: val.description,
                    quantity: 1,
                    price:val.price
                  })

                  
                  }}>
                      {plusIcon}
                  </Button>
                </div>
               </div>
            )
        })
   
    const {sentRequest: checkoutHandler} = useHttp();
    const {sentRequest: checkDeliveryInfoAvailable} = useHttp();

    function fetchedCheckoutResult(data){
      if(data.isSuccessful){
        setCheckoutResults({
          isSuccessful:data.isSuccessful,
          message:data.message
        })
        removeAllItems();
        navigate('/products/');
      }
    }
    

    function checkoutOrderHandler(){
      checkDeliveryInfoAvailable(
        {
        url: `${process.env.REACT_APP_API_URL}/users/deliveryinfo`,
        headers:{
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      },  
      deliveryinfoHandler)

      if(!deliveryInfoExist) {
      
        return;
      }

      const totalAmountHandler = cartOrders.map(val=>val.price * val.quantity);
      const finalTotal = totalAmountHandler.reduce((a,b)=>a+b,0);

      
      const updatedCartOrders = cartOrders.map(val=>
        {return {
          productId: val.id,
          productName: val.name,
          quantity: val.quantity
        }})

    
      

      let orderData = {
        products: updatedCartOrders,
        totalAmount:finalTotal
      }

      checkoutHandler(
        {url: `${process.env.REACT_APP_API_URL}/users/checkout`,
        method: "POST", 
        headers:{
        'Content-Type':'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
        },
        body: orderData},fetchedCheckoutResult)

    }

    const checkoutBtnAble = cartOrders?.length>0;

    return(
        <Offcanvas show={show} onHide={offcanvasControl}>
        {checkoutResults.isSuccessful !== null &&
        <div className="mb-3 alert alert-info text-center">Successful Checkout</div>}

        {(!deliveryInfoExist && deliveryInfoExist !==null) &&
        <div className="mb-3 alert alert-info text-center">Delivery Info needed! Kindly provide it
         <Link to="../profile" >here</Link></div>}

        <Offcanvas.Header closeButton>
          <Offcanvas.Title>Total Amount: Php {totalAmount}</Offcanvas.Title>
        </Offcanvas.Header>
        <Offcanvas.Body>
          {showOrder}
        </Offcanvas.Body>
        <Button variant='secondary' disabled={!checkoutBtnAble} onClick={checkoutOrderHandler}>
            checkout
        </Button>
      </Offcanvas>
    )
}