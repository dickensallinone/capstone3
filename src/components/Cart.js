import React from 'react';
import { useState,useContext } from 'react';
import { Button} from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCartShopping } from '@fortawesome/free-solid-svg-icons';

import AppContext from '../store/app-context';

import Checkout from './Checkout';
const cartElement = <FontAwesomeIcon icon={faCartShopping} />;

export default function Cart() {
  const {cartOrders} = useContext(AppContext);
  const [show, setShow] = useState(false);
  const [deliveryInfoExist,setDeliveryInfoExist]=useState(null);
  const numberOfCartOrders = cartOrders?.reduce((a,b)=>a+b.quantity,0);  

  function offcanvasControl(){
    setShow(show => !show)
}

function deliveryinfoHandler(deliveryAddressState){
    setDeliveryInfoExist(deliveryAddressState);
}

  return (
    <React.Fragment>
      <Button variant="dark" onClick={offcanvasControl}>
        {cartElement} {numberOfCartOrders}
      </Button>
      
      <Checkout 
      show={show} offcanvasControl={offcanvasControl}
        deliveryinfoHandler={deliveryinfoHandler}
        deliveryInfoExist={deliveryInfoExist}
      />
    </React.Fragment>
  );
}

