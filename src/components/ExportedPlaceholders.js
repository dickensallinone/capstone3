import {Col,Card, Placeholder } from "react-bootstrap";

export default function ExportedPlaceholder(props){
    const {breakPoint} = props;
    return(
        <Col lg={breakPoint} md={breakPoint +2} sm={breakPoint *2 +2}>
            <Card className="cardHighlight mx-auto">
            
                    <Card.Body>
                        <Placeholder as={Card.Title} animation="glow">
                            <Placeholder xs={6} />
                        </Placeholder>

                        <Placeholder as={Card.Text} animation="glow">
                            <Placeholder xs={7} /> <Placeholder xs={4} /> <Placeholder xs={4} />{' '}
                            <Placeholder xs={6} /> <Placeholder xs={8} />
                        </Placeholder>
                    </Card.Body>

                    <Placeholder as={Card.Footer} animation="glow">
                        <Placeholder xs={7} /> <Placeholder xs={4} /> <Placeholder xs={4} />{' '}
                        <Placeholder xs={6} /> <Placeholder xs={8} />
                        <Placeholder.Button xs={4} aria-hidden="true" />
                    </Placeholder>
                
            </Card>
        </Col>
       
    )
}