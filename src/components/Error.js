import { useRouteError } from "react-router-dom";
import Banner from "./Banner";
import AppContext from "../store/app-context";
import { useContext } from "react";
export default function Error() {
    const {user} = useContext(AppContext);
    const error = useRouteError();

    let title = "An error occured";
    let content = "Something went wrong";

    if(error.status === '404') {
        title = "404- Page not found";
        content = "The page you are looking for cannot be found"
    }

    if(error.status  === '500'){
        content = error.data.message;
    }
    return (
        <Banner header={title} 
        content={content}
        refRoute={`${user.id == null ? "products": "products"}`}
        buttonLabel="Back Home"
        />
    )
}