import React from "react";
import { Table,Row,Col,Button,Card} from "react-bootstrap";
import { Link } from "react-router-dom";
import EditProduct from "./EditProduct";
import DeactivateProduct from "./DeactivateProduct";

import '../index.scss';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlusCircle} from '@fortawesome/free-solid-svg-icons';
const plusElement = <FontAwesomeIcon icon={faPlusCircle} />;
export default function AdminView({productData}){
    const allProductDataTotal = productData?.length;
    const allActiveProductData = productData?.filter(val=>val.isActive )?.length || 0;
    const allInactiveProductData = productData?.filter(val=>!val.isActive)?.length || 0;
    
    const adminProducts = productData?.map((val)=>{
    
        return (
                
            <tr key={val._id} className={`product-row ${val.isActive ? 'table-info':'table-danger'}`} >
                <td className="product-font">{val._id}</td>
                <td className="product-font">{val.name}</td>
                <td className="product-font">{val.description}</td>
                <td className="product-font">Php {val.price}</td>
        
                <td>
                    <EditProduct 
                    product={val._id} /> 
                </td>

                <td>
                    <DeactivateProduct product={val._id} 
                    productActive={val.isActive}
                    />
                </td>
            </tr>
            
        )
    })


            return(
                <React.Fragment>
                    <Row className="d-flex align-items-center text-center my-4">
                        <Col lg={9}><h1>Product Inventory </h1></Col>
                        <Col lg={3}>
                            <Link to='addproduct'>
                            <Button variant="secondary">
                                {plusElement} Product
                            </Button>
                            </Link>
                        </Col>
                    </Row>

                    <Row className="text-center">
                        {[{title: 'Total Items',data:allProductDataTotal},
                        {title: 'Active Items',data: allActiveProductData},
                        {title: 'Not Active Items',data: allInactiveProductData}].map((val,i)=>(
                            <Col lg={4} 
                            md={6} sm={4} 
                            key={i} className="my-2">
                            <Card>
                                
                                <Card.Body>
                                <Card.Title>
                                    {val.title}
                                    </Card.Title>
                                    <Card.Text>
                                    {val.data}
                                    </Card.Text>
                                </Card.Body>
                            </Card>
                        
                        </Col>
                        ))}
                        
                    </Row>


                    <Table responsive  hover size="sm"
                     className="mb-1 table-borderless mt-5">
                    <thead >
                    <tr className="text-center">
                        <th>ID</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Price</th>
                      
                        <th colSpan={2}>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    {adminProducts}
                    </tbody>


                </Table>
                </React.Fragment>
                    
             )
    
}