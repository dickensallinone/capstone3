import { Nav,Spinner } from "react-bootstrap";
import { NavLink,useNavigation  } from "react-router-dom";
import ClientNavbarDesign from "../reusables/ClientNavbarDesign";
import Cart from "./Cart";
export default function UserNavBar(){
    const navigate = useNavigation();
    return (
        <ClientNavbarDesign>
         {navigate.state === 'loading'&&
         <Spinner animation="border" role="status">
        </Spinner>}
            <Nav variant="pills" className="me-auto">
                
                <Nav.Item>
                    <Nav.Link as={NavLink} to={'/products'} end>Products</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                    <Nav.Link as={NavLink} to={'order-history'} >Order History</Nav.Link>
                </Nav.Item>

                <Nav.Item>
                    <Nav.Link as={NavLink} to={'total-spent'}>Total Spent</Nav.Link>
                </Nav.Item>
                
        
                <Cart/>
            </Nav>
        </ClientNavbarDesign>
    )
}