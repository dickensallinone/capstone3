import { useState } from "react";
import { Button,FormGroup,FormLabel,FormControl,Form} from "react-bootstrap";

export default function DeliveryInfo({userDetails}){

    const {firstName:fname,
        lastName:lname,
        phoneNo:phoneNum,
        street:st,
        barangay:brgy,
        city:cityName,
        province:prov} = userDetails[0] || {};

    const [firstName,setFirstName] = useState(fname || '');
    const [lastName,setLastName] = useState(lname || '');
    const [phoneNo,setPhoneNo] = useState(phoneNum || "");
    const [street,setStreet] = useState(st || "");
    const [barangay,setBarangay] = useState(brgy || "");
    const [city,setCity] = useState(cityName || "");
    const [province,setProvince] = useState(prov || "");

    const deliveryInfo = {
        firstName: firstName,
        lastName: lastName,
        street: street,
        barangay: barangay,
        city: city,
        province: province,
        phoneNo: phoneNo
    }
    async function updateDeliveryInfo(e){

        e.preventDefault();

        const action = await fetch(`${process.env.REACT_APP_API_URL}/users/adddeliveryinfo`,{
            method: "POST",
            headers:{
                'Content-Type':'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify(deliveryInfo)
        })


        if(!action.ok) {
            return;
        }




    }
    return(
          <Form onSubmit={updateDeliveryInfo}>
            <Form.Text className="text-center">Your Delivery Information</Form.Text>
            <FormGroup controlId="first-name">
                    <FormLabel>Name: </FormLabel>
                    <FormControl 
                        type="text" 
                        placeholder="Enter your First Name"
                        value={firstName}
                        onChange={(e) => setFirstName(e.target.value)}
                        required
                        name="first-name"
                    />
                </FormGroup>

             <FormGroup controlId="last-name">
                 <FormLabel>Name: </FormLabel>
                 <FormControl 
                     type="text" 
                     placeholder="Enter your Last Name"
                     value={lastName}
                     onChange={(e) => setLastName(e.target.value)}
                     required
                     name="last-name"
                 />
             </FormGroup>
             
    
          <FormGroup controlId="phone-number">
                 <FormLabel>Phone No. </FormLabel>
                 <FormControl 
                     type="number" 
                     placeholder="Enter Phone No."
                     value={phoneNo}
                     onChange={(e) => setPhoneNo(e.target.value)}
                     name="phone-number"
                     required
                 />
             </FormGroup>
  
             <FormGroup controlId="street-name">
                 <FormLabel>Street</FormLabel>
                 <FormControl 
                     type="text" 
                     placeholder="Street Name"
                    value={street}
                    onChange={(e) => setStreet(e.target.value)}
                    name="street-name"
                     required
                 />
             </FormGroup>


             <FormGroup controlId="barangay">
                 <FormLabel>Street</FormLabel>
                 <FormControl 
                     type="text" 
                     placeholder="Barangay"
                      value={barangay}
                      onChange={(e) => setBarangay(e.target.value)}
                      name="barangay"
                     required
                 />
             </FormGroup>
  
             <FormGroup controlId="city">
                 <FormLabel>Town/City</FormLabel>
                 <FormControl 
                     type="text" 
                     placeholder="Town or City"
                      value={city}
                      onChange={(e) => setCity(e.target.value)}
                      name="city"
                     required
                 />
             </FormGroup>
  
             <FormGroup controlId="province">
                 <FormLabel>Province</FormLabel>
                 <FormControl 
                     type="text" 
                     placeholder="Province"
                      value={province}
                      onChange={(e) => setProvince(e.target.value)}
                      name="province"
                     required
                 />
             </FormGroup>
         
            <Button variant="primary" type='submit'>
              Save Changes
            </Button>
          
         </Form>      
    
    )
}
