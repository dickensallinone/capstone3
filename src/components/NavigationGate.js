import React from "react";
import { Outlet} from "react-router-dom";// import { Routes,Route } from "react-router-dom";
import AppNavbar from "./AppNavBar";
export default function NavigationGate(){
    
    return(
        <React.Fragment>
            <AppNavbar/>
            <Outlet/>
        </React.Fragment>
    )
}