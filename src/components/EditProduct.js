import { useState } from "react";
import { Button, Form, Modal } from "react-bootstrap";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit} from '@fortawesome/free-solid-svg-icons';
import useHttp from "../hooks/use-http";
const editElement = <FontAwesomeIcon icon={faEdit} />;

export default function EditProduct({product}){ 
    
    const[name,setName] = useState('');
    const[description,setDescription] = useState('');
    const [price,setPrice] = useState('');

    const [showEdit,setShowEdit] = useState(false); 

    const{sentRequest: fetchProductData} = useHttp();
    
    function openEdit(productId) {
        fetchProductData({url: `${process.env.REACT_APP_API_URL}/products/${productId}`},fetchProductDataHandler)
        setShowEdit(true)
    }

    function fetchProductDataHandler(data){
        const {name,description,price} = data;
        setName(name);
        setDescription(description);   
        setPrice(price)
    }

    function closeEdit(){
        setShowEdit(false);
        setName('');
        setDescription('');
        setPrice('')
    }

    const {sentRequest:updateProduct} = useHttp();

    function editCourse(e,productId){
        e.preventDefault();

        updateProduct({
        url: `${process.env.REACT_APP_API_URL}/products/${productId}/updateproduct`,
        method: "PUT",
        headers:{
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
        },
        body: {
            name: name,
            description: description,
            price: price
        }},updateProductHandler)

    }
    function updateProductHandler(data){
        if(data){
            closeEdit();
        }
        closeEdit()

    }
    return(
        <>
            <Button variant="primary" size="sm" onClick={()=>openEdit(product)}>{editElement}</Button>
            <Modal show={showEdit} onHide={closeEdit} className="edit-product__container">
                <Form onSubmit={(e)=>editCourse(e,product)}>
                    <Modal.Header closeButton>
                        <Modal.Title>
                            Edit Course
                        </Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <Form.Group controlId="courseName">
                            <Form.Label>Name</Form.Label>
                            <Form.Control type="text"
                            value={name}
                           onChange={(e)=>setName(e.target.value)}
                           name="name" required/>
                        </Form.Group>

                        <Form.Group controlId="courseDescription">
                            <Form.Label>Description</Form.Label>
                            <Form.Control type="text"
                            as="textarea"
                             value={description}
                            onChange={(e)=>setDescription(e.target.value)} 
                            name="description"
                            required/>
                        </Form.Group>

                        <Form.Group controlId="coursePrice">
                            <Form.Label>Price</Form.Label>
                            <Form.Control type="number"
                            value={price}
                            onChange={(e)=>setPrice(e.target.value)}
                            name="price" 
                            required/>
                        </Form.Group>
                    </Modal.Body>

                    <Modal.Footer>
                        <Button variant="success" type="submit" >Submit</Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </>
    )
}

export async function updateProductActionHandler({request,params}){

    // const data = await request.formData();
    // const loginData = {
    //     name: data.get('email'),
    //     description: data.get('password'),
    //     price: data.get('price')
    // }
}