import {Navbar, Nav, Container} from 'react-bootstrap';
import { Link, NavLink,useRouteLoaderData  } from 'react-router-dom';
import '../index.scss';

export default function AppNavbar(){

	const token = useRouteLoaderData('root');

	return (
		<Navbar expand="md" className='appnavbar'>
			<Container >
			    <Navbar.Brand as={Link} to={'/'}>store-U</Navbar.Brand>
			
			    <Navbar.Toggle aria-controls="basic-navbar-nav"/>
			    <Navbar.Collapse id="basic-navbar-nav">
			    	<Nav className="ms-auto">
											
						 {token === null&& 
						 <>
						 	<Nav.Link as={NavLink} to="products">Products</Nav.Link>
							<Nav.Link as={NavLink} to="/login">Login</Nav.Link>
						 	<Nav.Link as={NavLink} to="/register">Register</Nav.Link>
						 </>}
						{(token !== null && token !== undefined)&&
						<>
							<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
							<Nav.Link as={NavLink} to="profile">Profile</Nav.Link>
						</>
						}						
			    	</Nav>
			    </Navbar.Collapse>
			</Container>
		</Navbar>
	)
}