import { useContext} from "react";
import { Col,Card,Button, Row } from "react-bootstrap";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faExpand } from '@fortawesome/free-solid-svg-icons';
import { Link } from "react-router-dom";
import AppContext from "../store/app-context";
import '../index.scss'
// const heartElement = <FontAwesomeIcon icon={faHeart} />;

const expandElement = <FontAwesomeIcon icon={faExpand} />

export default function ProductItem({productSpecific}){
    const {addItem,user} = useContext(AppContext);
    const {_id,name,description,price} = productSpecific;
    return(
        <Col className="mb-3 g-4" lg={4} md={6} sm={12}>
            <Card className="product-item shadow-sm">
                <Card.Header>
                    <Row className="justify-content-around text-center">
                    <Card.Title className="col-6">{name}</Card.Title>
                     <Link to={`/products/${_id}`} className="col-6"><Button variant="secondary">
                        {expandElement}
                     </Button></Link>
                    </Row>
                </Card.Header>
                <Card.Body>
                    <Card.Text>
                        {description}
                    </Card.Text>
                    <Card.Text>PHP {price}</Card.Text>
                </Card.Body>

                {!user.isAdmin &&
                    <Card.Footer>
                    <Row className="justify-content-around text-center">
                        <Col className="col-6">
                        <Button variant="primary" onClick={()=>{
                            addItem({
                            id: _id,
                            name: name,
                            description:description,
                            quantity: 1,
                            price:price
                        })
                        }}>Add to Cart</Button>
                        </Col>

                        {/* <Col className="col-6">
                        <Button variant="dark" >
                            {heartElement}
                            </Button>
                        </Col>
                         */}
                    </Row>
                </Card.Footer>}
            </Card>
        </Col>
    )
}

