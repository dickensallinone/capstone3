import { useCallback, useState} from "react";

const useHttp = ()=>{
    const [loading,setLoading] = useState(false);
    const [error,setError] = useState('');
    const sentRequest= useCallback(
        async function (requestConfig,fetchData){
            setLoading(true);
            try{
                const data = await fetch(requestConfig.url,{
                    method: requestConfig.method ? requestConfig.method: "GET",
                    headers: requestConfig.headers ? requestConfig.headers : {},
                    body: requestConfig.body ? JSON.stringify(requestConfig.body) : null,
                    Authorization: requestConfig.authorization? requestConfig.authorization : null
                });
    
                if(!data.ok) {
                    throw new Error('Request Failed!')
                }
                const finalData = await data.json();
                 fetchData(finalData);
            }catch(err){
                setError(err.message || "Something went wrong")
            }
            setLoading(false); 
        },[])
    
    return {
        loading,
        error,
        sentRequest
    }
}

export default useHttp;