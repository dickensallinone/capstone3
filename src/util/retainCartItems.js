

export async function addToCartHandler(){
    const updateCart = await fetch(`${process.env.REACT_APP_API_URL}/users/addtocart`,{
      method: "GET",
      headers:{
      'Content-Type': 'application/json',
      Authorization: `Bearer ${localStorage.getItem('token')}`}
    }) 

    if(!updateCart.ok){
      return "something went wrong"
    }

    const result = await updateCart.json();

    return result;
  }   
