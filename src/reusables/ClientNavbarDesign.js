import { Navbar,Container } from "react-bootstrap";
import '../index.scss';

export default function ClientNavbarDesign(props){
    return(
        <Container fluid="sm">
        <Navbar bg="dark" data-bs-theme="dark"
        className="client-navbar px-5 py-2">
                {props.children}
            
        </Navbar>
        </Container>
    )
}