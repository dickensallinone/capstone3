import React from "react";
import { Row, Container,Col } from "react-bootstrap";
import '../index.scss';

export default function LoginRegisterFormBody(props){
    return(
        <Container >
            <Row>
                <Col lg={5} md={5} sm={8} 
                className="mx-auto form-body">
                    {props.children}
                </Col>
            </Row>
        </Container>
    )
}