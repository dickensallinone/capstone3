import {createBrowserRouter,RouterProvider} from 'react-router-dom';
import AppProvider from './store/AppProvider';
import NavigationGate from './components/NavigationGate';
import './index.scss';
import HomePage from './pages/HomePage';
import Error from './components/Error';
import Products from './pages/Products';
import LoginPage from './pages/LoginPage';
import RegisterPage from './pages/RegisterPage';
import Logout from './pages/Logout';
import RetrieveUsersOrdersPage from './pages/RetrieveUsersOrdersPage';
import NavigationSetup from './components/NavigationSetup';
import TotalSpentPage from './pages/TotalSpentPage';
import ProductView from './pages/ProductView';
import UsersPage from './pages/UsersPage';
// import Activities from './pages/Activities';
import AddProduct from './pages/AddProduct';
import ProfilePage from './pages/ProfilePage';
import OrderHistoryPage from './pages/OrderHistoryPage';
import { productsLoaderHandler } from './pages/Products';
import { productViewLoaderHandler } from './pages/ProductView';
import { tokenLoaderHandler } from './util/auth';
import { loginActionHandler } from './pages/LoginPage';
import { loadOrderHistory } from './pages/RetrieveUsersOrdersPage';
import { loadTotalSpent } from './pages/TotalSpentPage';
import { loadProfile } from './pages/ProfilePage';
import { loaderHomePage } from './pages/HomePage';
import { registerUserHandler } from './pages/RegisterPage';
import { orderedUserProductsLoader } from './pages/OrderHistoryPage';
import { getAllUsersLoader } from './pages/UsersPage';
const router = createBrowserRouter([
  {
    path: '/',
    element: <NavigationGate/>,
    errorElement: <Error/>,
    id: 'root',
    loader: tokenLoaderHandler,
    children:[
      {index: true, element:<HomePage/>,loader: loaderHomePage},
      {path: 'products', element: <NavigationSetup/>,
      children:[
        {index: true, element: <Products/>,
        loader: productsLoaderHandler,
        errorElement:<Error/>
        },
        {path: 'order-history', element: <RetrieveUsersOrdersPage/>,
      loader: loadOrderHistory},
        {path: 'total-spent', element: <TotalSpentPage/>,loader:loadTotalSpent},
        // {path: 'product-statistics', element: <ProductStatistics/>},
        {path: 'users-orders', element: <OrderHistoryPage/>,
      loader:orderedUserProductsLoader},
        {path: 'users', element: <UsersPage/>,loader:getAllUsersLoader},
        // {path: 'activities', element: <Activities/>},
        {path: 'addproduct',element:<AddProduct/>},
        {path: ':productId',
        id:'product-detail', 
        loader:productViewLoaderHandler,
        children:[
          {index:true,
          element: <ProductView/>}
        ]}
      ]},
      {path: 'login', 
      element: <LoginPage/>,
      action: loginActionHandler},
      {path: 'register', element: <RegisterPage/>,
    action:registerUserHandler},
      {path: 'logout', element: <Logout/>},
      {path: 'profile',
      children:[
        {
          index: true,
          element: <ProfilePage/>,
          loader: loadProfile
        }
      ]}
    ]
  }
])

function App() {

  return (
       <AppProvider>
        <RouterProvider router={router}/>        
       </AppProvider>

  );
}

export default App;
