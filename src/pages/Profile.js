import React, { useState} from "react";
import { Row,Col,Container,Stack, Button} from "react-bootstrap";
import DeliveryInfo from "../components/DeliveryInfo";
import UpdateCredentials from "../components/UpdateCredentials";
import { useLoaderData } from "react-router-dom";

import '../index.scss';

export default function Profile(){
    const data = useLoaderData();

    const {email,details} = data[0];
    const [updateCred,setUpdateCred] = useState(false);
    function toggleUpdateCredForm(){
        setUpdateCred(updateCred ? false : true);

    }

    return (
        <Container className="profile-container mt-3">
             <Row >
                <Col lg={3}>
                   {!updateCred &&  <Stack gap={3}>
                        <div className="p-2">Email Address: {email}</div>
                        <div className="p-2">Password: ********</div>

                        <Button variant="secondary" onClick={toggleUpdateCredForm}>Update </Button>
                    </Stack>}


                    {updateCred && <UpdateCredentials toggleUpdateCredForm={toggleUpdateCredForm}/>}
                    
                </Col>
            </Row>

   
            <DeliveryInfo userDetails={details}/>
        </Container>
           
        
    )
}