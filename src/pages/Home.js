import React,{useContext} from 'react';
import Banner from '../components/Banner.js';
import Featured from '../components/Featured.js';
import NavigationSetup from '../components/NavigationSetup.js';
import AppContext from '../store/app-context.js';
import { Container } from 'react-bootstrap';

export default function Home({data}){
	const {user} = useContext(AppContext);
	return(
		<Container className='my-2'>
			<NavigationSetup/>
			  {!user.isAdmin && <Banner header="Welcome to Store-U" 
        content="Best Store for Best Prices!"
        refRoute="/products"
		buttonLabel="Buy Now!"
        />}
			<Featured data={data}/>
		</Container>
	)
}