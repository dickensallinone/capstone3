import { useContext} from "react";
import { Link,useRouteLoaderData,json } from "react-router-dom";
import { Container,Card,Button,Row,Col } from "react-bootstrap";
import { useParams} from "react-router-dom";
import AppContext from "../store/app-context";
import '../index.scss';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowAltCircleLeft } from "@fortawesome/free-solid-svg-icons";
const backToProducts = <FontAwesomeIcon icon={faArrowAltCircleLeft}/>

export default function ProductView(){
    const {user} = useContext(AppContext);
    console.log(user);
    const {productId} = useParams();
    const productDetail = useRouteLoaderData('product-detail');

    const {name,description,price} = productDetail;
 
    return(
        <Container className="mt-3">
                    <Link to=".." relative="path">
                        <Button variant="dark">
                            {backToProducts}
                        </Button>
                    </Link> 
            <Row className="mt-5">
                <Col lg={6} className="mx-auto">
                <Card key={productId} className="course-view">
                    <Card.Header as="h5">{name}</Card.Header>
                    <Card.Body>
                    
                        <Card.Text>
                        {description}
                        </Card.Text>

                        <Card.Text>
                        Php {price}
                        </Card.Text>
                        {user.id !== null  ? <Button variant="primary" >Add Cart</Button> :
                        <Link to="/login" variant="secondary">Login To Checkout</Link>}
                    </Card.Body>
                </Card>
                </Col>
            </Row>
        </Container>
    )
}

export async function productViewLoaderHandler({request,params}){
    const productId = params.productId;
    const result = await fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`);

    if(!result.ok) {
        return json({message: "Product Details Cannot be Displayed"},
        {status: 500})
    }

    return await result.json();
}