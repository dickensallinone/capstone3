import React from "react";
import Register from "./Register"
import { json } from "react-router-dom";
export default function RegisterPage(){
    return (
        <React.Fragment>
            <Register/>
        </React.Fragment>
      
    )
}


export async function registerUserHandler({request,params}){
    const data = await request.formData();
    let registerUserStatus;
    let isUserExist;
    const registerData = {
        email: data.get('email'),
        password: data.get('password')
    }    
    try{
       const data = await fetch(`${process.env.REACT_APP_API_URL}/users/checkemailexists`,{
        method: "POST",
        headers: {
            'Content-Type': 'application/json'
        },
        body:JSON.stringify(registerData)

       });

        if(!data.ok || data.status === 400 ||
            data.status === 404) {
                isUserExist = false;
            }

        isUserExist = await data.json();
    }catch(err) {
        console.log(err.message);
    }
   

    if(isUserExist) {
        return {
            isSuccessful: false,
            message: "User already exist"
        }
    }
    try{
        const data = await fetch(`${process.env.REACT_APP_API_URL}/users/register`,{
        method: "POST",
        headers: {
            'Content-Type': 'application/json'
        },
        body:JSON.stringify(registerData)});

        if(!data.ok || data.status === 400 ||
            data.status === 404) {
                throw json({message: "Something went wrong"},
                {status: 500})
            }

        registerUserStatus= await data.json();
    }catch (error) {
        console.log(error)
    }

    return registerUserStatus;

}