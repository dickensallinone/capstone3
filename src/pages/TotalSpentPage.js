import React from "react";
import TotalSpent from "./TotalSpent";
export default function TotalSpentPage(){
    return(
        <React.Fragment>
            <TotalSpent/>
        </React.Fragment>
    )
}

export async function loadTotalSpent(){
    const data = await fetch(`${process.env.REACT_APP_API_URL}/users/retrieveusersorders`,{
        headers: {
            'Content-Type':'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }
    });

    if(data.ok) {
        return data.json(); 
    }

}