import { Container,Row } from "react-bootstrap";
import UserItem from "../components/UserItem";
import { useLoaderData } from "react-router-dom"

export default function Users(){
    const data = useLoaderData();

    const displayData = data?.map(val=>{
        const {personalInfo,_id:id} = val;

        const {firstName="",lastName=""} = personalInfo[0];
        console.log(personalInfo)
    
          return (  <UserItem key={id} id={id} firstName={firstName}
        lastName={lastName}/>)
        
        
    
    })
    return(
        <Container fluid>
            <Row>
                {displayData}
            </Row>
        </Container>
    )
}