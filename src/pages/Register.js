import React,{useState,useReducer, useEffect} from "react";
import { Button,FormGroup,FormLabel,FormControl,FormText,Spinner } from "react-bootstrap";
import LoginRegisterFormBody from "../reusables/LoginRegisterFormBody";
import '../index.scss';
import { useActionData,Form,useNavigate } from "react-router-dom";

const emailReducer = (state,action)=>{
    if(action.type==="USER_INPUT"){
        return {
            val: action.val,
            isValid: action.val.includes('@')
        }
    }

    if(action.type==="INPUT_BLUR"){
        return {
            val: state.val,
            isValid: state.val.includes('@')
        }
    }
    return{
        val: "",
        isValid: false
    }
}

const passwordReducer = (state,action)=>{
    if(action.type==="USER_INPUT"){
        return {
            val: action.val,
            isValid: action.val?.trim()?.length > 6
        }
    }

    if(action.type==="INPUT_BLUR"){
        return {
            val: state.val,
            isValid: state.val?.trim()?.length > 6
        }
    }
    return{
        val: "",
        isValid: false
    }
}

const confirmPasswordReducer = (state,action)=>{
    if(action.type==="USER_INPUT"){
        return {
            val: action.val,
            isValid: action.val?.trim()?.length > 6
        }
    }

    if(action.type==="INPUT_BLUR"){
        return {
            val: state.val,
            isValid: state.val?.trim()?.length > 6
        }
    }
    return{
        val: "",
        isValid: false
    }
}

export default function Register(){
    const resData = useActionData();
    const navigate = useNavigate();
    const stateSubmission = navigate.state === 'submitting';
    const [isFormValid,setIsFormValid] = useState(false);

    const [email,dispatchEmail] = useReducer(emailReducer,{
        val: '',
        isValid: null
    })

    const [password,dispatchPassword] = useReducer(passwordReducer,{
        val:'',
        isValid: null
    })

    const [confirmPassword,dispatchConfirmPassword] = useReducer(confirmPasswordReducer,{
        val:'',
        isValid: null
    })

    function emailValueHandler(e) {
        dispatchEmail({type:"USER_INPUT",val:e.target.value})
    }

    function validateEmailHandler(){
        dispatchEmail({type:"INPUT_BLUR"})
    }

    function passwordValueHandler(e){
        dispatchPassword({type: "USER_INPUT",val:e.target.value})
    }

    function validatePasswordHandler(){
        dispatchPassword({type: "INPUT_BLUR"})
    }

    function confirmPasswordValueHandler(e){
        dispatchConfirmPassword({type:"USER_INPUT",val:e.target.value})
    }

    function validateConfirmPasswordHandler(){
        dispatchConfirmPassword({type: "INPUT_BLUR"})
    }
   
    const {isValid:emailValid} = email;
    const {isValid:passwordValid} = password;
    const {isValid:confirmPasswordValid} = confirmPassword;
    useEffect(()=>{
        setIsFormValid(
            emailValid && passwordValid && confirmPasswordValid && password.val === confirmPassword.val
        )
    },[emailValid,passwordValid,confirmPasswordValid,
    email.val,password.val,confirmPassword.val])
  
        const {isSuccessful,message} = resData || {isSuccessful: null,message: ""};

        const classNames = `mb-3 alert alert-info text-center ${isSuccessful? '': 'text-danger'}`
    return(
        <LoginRegisterFormBody>
            {(!isSuccessful && isSuccessful !== null) && 
            <div className={classNames}>
                {message}
            </div>}

            {stateSubmission && <Spinner animation="border" />}
          
            
             <Form method="POST">
                <FormGroup controlId="email mb-3">
                    <FormLabel>Email address</FormLabel>

                    <FormControl type="email" 
                    placeholder="Enter email" 
                    value={email.val}
                    onChange={emailValueHandler}
                    onBlur={validateEmailHandler}
                    name="email"
                    />
                    {(email.isValid !== null && !email.isValid) &&
                        <FormText className="text-muted">  
                    Invalid Email Address
                    </FormText>}            
                </FormGroup>

                <FormGroup controlId="password" className="mb-3">
                    <FormLabel>Password</FormLabel>

                    <FormControl type="password" 
                    placeholder="Password" 
                    value={password.val}
                    onChange={passwordValueHandler}
                    onBlur={validatePasswordHandler}
                    name="password"
                    />
                    {(password.isValid !== null && !password.isValid) &&
                        <FormText className="text-muted">  
                        Error with your password
                    </FormText>}   
                </FormGroup>

                <FormGroup controlId="confirmPassword">
                    <FormLabel>Confirm Password</FormLabel>
                    <FormControl type="password" 
                    placeholder="Confirm Password" 
                    value={confirmPassword.val}
                    onChange={confirmPasswordValueHandler}
                    onBlur={validateConfirmPasswordHandler}
                    />

                    {((confirmPassword.isValid !== null && !confirmPassword.isValid) || confirmPassword.val !== password.val) &&
                        <FormText className="text-muted">  
                        Password doesn't match
                    </FormText>}   
                    <FormText className="text-muted">
                    
                    </FormText>  
                </FormGroup>
            
                <FormGroup className="text-center mt-3">
                    <Button variant="primary" type="submit" disabled={!isFormValid}>
                    {stateSubmission ? 'Registering' : 'Create Account'}
                    </Button>
                </FormGroup>
            </Form>

            
           
        </LoginRegisterFormBody>
       
    )
}