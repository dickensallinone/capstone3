import { useContext } from "react";
import AppContext from "../store/app-context";
import UserView from "../components/UserView";
import AdminView from "../components/AdminView";
import { Container } from 'react-bootstrap';
import { useLoaderData, json } from 'react-router-dom';
import '../index.scss';

export default function Products(){
    const resultData = useLoaderData();
    const {user} = useContext(AppContext);

    let val = user.isAdmin !== null && !user.isAdmin;
    
    const productDisplay = val ? <UserView productData={resultData} /> :
    <AdminView productData={resultData} 
    />;

    
    return(
        <Container className="product-container">
            {productDisplay}
        </Container>
    )
}

export async function productsLoaderHandler(){
    const result = await fetch(`${process.env.REACT_APP_API_URL}/products/allproducts`);

    if(!result.ok) {
        return json({message: 'Could not fetch products'},{
            status: 500
        });
    }

    return await result.json();
}