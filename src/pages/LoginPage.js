import React from "react";
import Login from "./Login";
import { json} from "react-router-dom";
export default function LoginPage(){

    return (
        <React.Fragment>
            <Login />
        </React.Fragment>
    )
}

export async function loginActionHandler({request,params}){
    const data = await request.formData();
    const loginData = {
        email: data.get('email'),
        password: data.get('password')
    }
    const result = await fetch(`${process.env.REACT_APP_API_URL}/users/login`,{
        method: "POST",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(loginData)
    })


    if(result.status === 422 || result.status === 401){
    
        return result;
    }

    else if(!result.ok) {
        throw json({message: "Something went wrong"},
        {status: 500})
    }

    const resData = await result.json();
    const token = resData?.accessToken;
    if(!token || token === undefined ||
        token === null) {
        return resData;
    
    }else {
        localStorage.setItem('token',token);
       
        return resData;
    }

    
}