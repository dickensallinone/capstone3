import React from "react";
import { json,redirect } from "react-router-dom";
import Users from "./Users";
export default function UsersPage(){
    return (
        <React.Fragment>
            <Users/>
        </React.Fragment>
    )
    }
export async function getAllUsersLoader(){

    const token = localStorage.getItem("token");

    if(token === null) {
        return redirect("/login");
    }
    let resData;
    try{
        const data =  await fetch(`${process.env.REACT_APP_API_URL}/users/allusers`,{
            headers: {
                'Content-Type':'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        });

        if(!data.ok ||
            data.status === 404) {
            throw new json({message: "Something went wrong"},{status: 500})
        }

        resData = await data.json();
    }catch(error) {
        return error.message
    }

    return resData;
}