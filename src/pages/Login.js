import { Button,FormGroup,FormLabel,FormControl} from "react-bootstrap";
import { useState,useReducer,useEffect,useContext} from "react";
import LoginRegisterFormBody from "../reusables/LoginRegisterFormBody";
import AppContext from "../store/app-context";
import { useNavigation,Form, useActionData,useNavigate,Navigate} from 'react-router-dom';
import { addToCartHandler } from "../util/retainCartItems";
import '../index.scss';

const emailReducer = (state,action)=>{
    if(action.type==="USER_INPUT"){
        return {
            val: action.val,
            isValid: action.val.includes('@')
        }
    }

    if(action.type==="INPUT_BLUR"){
        return {
            val: state.val,
            isValid: state.val.includes('@')
        }
    }
    return{
        val: "",
        isValid: false
    }
}

const passwordReducer = (state,action)=>{
    if(action.type==="USER_INPUT"){
        return {
            val: action.val,
            isValid: action.val?.trim()?.length > 6
        }
    }
    
    if(action.type==="INPUT_BLUR"){
        return {
            val: state.val,
            isValid: state.val?.trim()?.length > 6
        }
    }
    return{
        val: "",
        isValid: false
    }
}

export default function Login(){
    const {obtainTokenUponLogin,retainItems} = useContext(AppContext);
    const [isFormValid,setIsFormValid] = useState(false);
    const navigate = useNavigation();
    const resData = useActionData();
    const navigatePage = useNavigate();
   
    const stateSubmission = navigate.state === 'submitting';
    const [email,dispatchEmail] = useReducer(emailReducer,{
        val: '',
        isValid: null
    })
    const [password,dispatchPassword] = useReducer(passwordReducer,{
        val:'',
        isValid: null
    })

    function emailValueHandler(e) {
        dispatchEmail({type:"USER_INPUT",val:e.target.value})
    }

    function validateEmailHandler(){
        dispatchEmail({type:"INPUT_BLUR"})
    }

    function passwordValueHandler(e){
        dispatchPassword({type: "USER_INPUT",val:e.target.value})
    }

    function validatePasswordHandler(){
        dispatchPassword({type: "INPUT_BLUR"})
    }


     useEffect(()=>{
        setIsFormValid(
            email.isValid && password.isValid
        )

    },[email.isValid,password.isValid,
    email.val,password.val]);

    useEffect(()=>{
        if(resData?.isValid) {
            async function fetchAnother(){
                const fetchedData =  await addToCartHandler();
                retainItems({items:fetchedData.items || [],
                 totalAmount:fetchedData.totalAmount || 0});
               } 
        
               fetchAnother();
                obtainTokenUponLogin(resData?.accessToken);
          
        }else {
            return;
        }   
    },[resData,navigatePage,obtainTokenUponLogin,retainItems])


    return(
        (resData?.isValid) ? <Navigate to="/products" replace={true}/> : 
        <LoginRegisterFormBody>
            {(!resData?.isValid && resData !== undefined) && <div className="mb-3 alert alert-info text-center">
                Error with the credentials. Please try again!
            </div>}
           
            <Form method="post">  
                <FormGroup controlId="formBasicEmail">
                    <FormLabel>Email address</FormLabel>
                    <FormControl type="email"
                    placeholder="Enter email"
                    name="email"
                    value={email.val}
                    onChange={emailValueHandler}
                    onBlur={validateEmailHandler} />
                  
                </FormGroup>

                <FormGroup controlId="formBasicPassword">
                    <FormLabel>Password</FormLabel>
                    <FormControl type="password" 
                    placeholder="Password"
                    name="password" 
                    value={password.val}
                    onChange={passwordValueHandler}
                    onBlur={validatePasswordHandler} 
                    />
                </FormGroup>
                
                <FormGroup controlId="formSubmitBtn" className="text-center mt-3">
                    <Button variant="dark" type="submit" disabled={!isFormValid}>
                        {stateSubmission ? 'Logging in' : 'Login'}
                    </Button>
                </FormGroup>
            </Form>

            
        </LoginRegisterFormBody>
    )
}

export async function loginLoader(){
    
}