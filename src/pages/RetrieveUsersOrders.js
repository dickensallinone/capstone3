import React from "react";
import { Col, Container, Row, Card} from "react-bootstrap";
import { useLoaderData } from "react-router-dom";
import OrderedProductsList from "../components/OrderedProductsList";
export default function OrderedProducts(){
    const data = useLoaderData();

    function formatDate(date){

        const formatDate = new Date(date);

        return formatDate.toLocaleDateString();
    }

    const displayData = data.order.map(item=>(
        <Col key={item._id} lg={6}>
            <Card >
            <Card.Header>{formatDate(item.purchasedOn)}</Card.Header>
            <Card.Body>
                <Card.Title>P {item.totalAmount}</Card.Title>
                <OrderedProductsList list={item.products} keyValue={item._id}/>
            </Card.Body>
        </Card>

      
        </Col>
    ))
    return(
        <Container>
            

            <Row className="mt-5">
                {displayData}
            </Row>
        </Container>
    )
}
