import React from "react";
import Profile from "./Profile";
import { json,defer, redirect } from "react-router-dom";
export default function ProfilePage(){
    return(
        <React.Fragment>
            <Profile/>
        </React.Fragment>
    ) 
}

export async function loadProfile(){
    const data = await fetch(`${process.env.REACT_APP_API_URL}/users/profile`,{
        headers: {
            'Content-Type':'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }
    });

    const token = localStorage.getItem("token");

    if(!token) {
        return redirect('/login');
    }


    if(!data.ok) {
        throw new json({message: "Something went wrong"},{status: 500})
    }

    const resultData = await data.json();

    return resultData;
}

export function loadProfileData(){

    defer({
        loadProfile: loadProfile
    })
}