import { useContext,useEffect } from 'react';
import { Navigate } from 'react-router-dom';
import AppContext from '../store/app-context';

export default function Logout() {
	const {invalidLogin} = useContext(AppContext);
	
	function clearingToken(){
		return localStorage.clear();
	  }

	clearingToken();

	useEffect(()=>{
		invalidLogin()
		
	},[invalidLogin]);

	return (
		//Redirect the user back to the login page.
		<Navigate to="/login" />
	)

}