import React from "react";
import { json,redirect } from "react-router-dom";
import OrderHistory from "./OrderHistory";

export default function OrderHistoryPage(){
    return (
        <React.Fragment>
            <OrderHistory/>
        </React.Fragment>     
    )
}


export async function orderedUserProductsLoader(){

    const token = localStorage.getItem("token");

    if(token === null) {
        return redirect("/login");
    }
    
    const data = await fetch(`${process.env.REACT_APP_API_URL}/products/list/usersorderedproducts`,{
        headers: {
            'Content-Type':'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }
    });

    if(!data.ok ||
        data.status === 404 ||
        data.status === 500){
            throw json({message: "Something went wrong"},
            {status: 500})
        }

    const resultData = await data.json();

    return resultData;
}