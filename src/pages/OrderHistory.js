import { useLoaderData } from "react-router-dom";
import { Card,Row,Col } from "react-bootstrap";
export default function OrderHistory(){
    const data = useLoaderData();
    const {order} = data;

    const mappedProducts = order.map(val=>{
        return (
            <Col key={val.productId} lg={6}>
                <Card>
                    <Card.Body>
                    <Card.Title>{val.productName}</Card.Title>
                    <Card.Text>
                     {val.quantity}
                    </Card.Text>
                   
                    </Card.Body>
                </Card>
            </Col>
        )
    })

    return (
        <Row className="container">
            {mappedProducts}
        </Row>
    )
}