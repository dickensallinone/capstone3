import React from "react";
import { Link, useLoaderData } from "react-router-dom";
import { Card, Container, Row } from "react-bootstrap";
export default function TotalSpent(){
    const {order:data} = useLoaderData();
 
    let total = 0;

    if(data.length > 0){
        for (let i = 0; i < data.length; i++) {
            total+=data[i].totalAmount 
        }
    }
    return(
        <Container className="mt-5">
           <Card>
            <Card.Header>Total Spent </Card.Header>
            <Card.Body>
                <Card.Title>P {total}</Card.Title>
                <Card.Text>
                <Link to="../order-history" relative="path" variant="secondary">View Order History</Link>
                </Card.Text>
               
            </Card.Body>
            </Card>
            <Row>

            </Row>

        </Container>
    )
}