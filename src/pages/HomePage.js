import React, {Suspense} from "react";
import { json,defer,useLoaderData,Await } from "react-router-dom";
import Home from "./Home";

export default function HomePage(){
    const {featured} = useLoaderData();

    return (
        <Suspense fallback={<p>Loading...</p>}>
            <Await resolve={featured}>

                {(featured)=> <Home data={featured}/>}
            </Await>

        </Suspense>
    )
}

async function loadFeaturedProducts(){

    const data = await fetch(`${process.env.REACT_APP_API_URL}/products/allactiveproducts`);

    if(!data.ok) {

        return json({
            message: "Something went wrong!"
        })
    }

    return await data.json();
}

export function loaderHomePage(){

    return defer({
        featured: loadFeaturedProducts()
    });
}

