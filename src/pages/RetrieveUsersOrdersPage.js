import React from "react";
import RetrieveUsersOrders from "./RetrieveUsersOrders";
import { json } from "react-router-dom";
export default function OrderedProductsPage(){
    return (
        <React.Fragment>
            <RetrieveUsersOrders/>
        </React.Fragment>
    )
}

export async function loadOrderHistory(){

    const data = await fetch(`${process.env.REACT_APP_API_URL}/users/retrieveusersorders`,{
        headers: {
            'Content-Type':'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }
    });

    if(data.status === 404 ||
        !data.ok) {
            return json({
                message: "Something went wrong!"
            })
    }

    const resultData = await data.json();
    return resultData;
}