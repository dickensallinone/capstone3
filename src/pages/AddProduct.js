import { Form,Button,Spinner } from "react-bootstrap";
import { useState,useEffect, useContext, useReducer } from "react";
import AppContext from "../store/app-context";
import useHttp from "../hooks/use-http";
import LoginRegisterFormBody from '../reusables/LoginRegisterFormBody';
import { Link } from "react-router-dom";
import { useNavigate,Navigate} from "react-router-dom";

const defaultProductName={
    value: '',
    isValid: false
}
const productNameReducer = (state,action)=>{
    if(action.type==="PRODUCT_NAME") {
        return {value: action.value,isValid: action.value.trim().length > 4}
    }

    if(action.type==="PRODUCT_NAME_BLUR"){
        return {
            value: state.value,
            isValid: state.value.trim().length > 4
        }
    }

    return defaultProductName
}

const defaultProductDescription ={
    value: '',
    isValid: false
}

const productDescriptionReducer = (state,action)=>{
    if(action.type==="PRODUCT_DESCRIPTION") {
        return {value: action.value,isValid: action.value.trim().length >= 50}
    }

    if(action.type==="PRODUCT_DESCRIPTION_BLUR"){
        return {
            value: state.value,
            isValid: state.value.trim().length >= 10
        }
    }

    return defaultProductDescription
}

const defaultProductPrice = {
    value: 0,
    isValid: false
}

const productPriceReducer = (state,action)=>{
    if(action.type==="PRODUCT_PRICE") {
        return {value: action.value,isValid: action.value > 0}
    }

    if(action.type==="PRODUCT_PRICE_BLUR"){
        return {
            value: state.value,
            isValid: state.value > 0
        }
    }

    return defaultProductPrice
}

export default function AddProduct(){
    
    const {user} = useContext(AppContext);
    const [result,setResult] = useState(false);
    const [isActive,setIsActive] = useState(false);
    const [productName,dispatchProductName] = useReducer(productNameReducer,defaultProductName);
    const [productDescription,dispatchProductDescription] = useReducer(productDescriptionReducer,defaultProductDescription);
    const [productPrice,dispatchProductPrice] = useReducer(productPriceReducer,defaultProductPrice);

    const {loading,error,sentRequest: addProduct} = useHttp();

    
    function addProductHandler(e){
        e.preventDefault();

        addProduct({url:`${process.env.REACT_APP_API_URL}/products/createproduct`,
        method: "POST",
        headers:{
            'Content-Type':'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
        },
        body: {
            name: productName.value,
            description: productDescription.value,
            price: productPrice.value
        }},fetchCreateProductResult)
    }

   
    const navigate = useNavigate()
    function fetchCreateProductResult(data){
        if(data){

            setResult(data);
            dispatchProductName({type: 'PRODUCT_NAME',value: ''})
            dispatchProductDescription({type: 'PRODUCT_DESCRIPTION',value: ''})
            dispatchProductPrice({type: 'PRODUCT_PRICE',value: 0})
            navigate('/products/')
        }
    }

    function productNameInput(e){
        dispatchProductName({type: 'PRODUCT_NAME',value: e.target.value})
    }

    function productNameInputBlur(){
        dispatchProductName({type: 'PRODUCT_NAME_BLUR'})
    }

    function productDescriptionInput(e){
        dispatchProductDescription({type: 'PRODUCT_DESCRIPTION',value: e.target.value})
    }

    function productDescriptionInputBlur(){
        dispatchProductDescription({type: 'PRODUCT_DESCRIPTION_BLUR'})
    }

    function productPriceInput(e){
        dispatchProductPrice({type: 'PRODUCT_PRICE',value: e.target.value})
    }
    
    function productPriceInputBlur(){
        dispatchProductPrice({type: 'PRODUCT_PRICE_BLUR'})
    }

    useEffect(()=>{
        setIsActive(productPrice.isValid &&
            productDescription.isValid &&
            productPrice.isValid)
    },[productName.isValid,
    productDescription.isValid,
    productPrice.isValid,
    productName.value,
    productDescription.value,
    productPrice.value]);
    

    return(
        <>

           { !user.isAdmin ? <Navigate to="/courses"/>  :
            <LoginRegisterFormBody>
            {loading && <Spinner animation="border" />}
            {error && <div className="mb-3 alert alert-info text-danger">{error}</div>}
            {result && <div className="mb-3 alert alert-info text-center">Product Added Successfully</div>}
            <Link to=".." relative="path">Back</Link>
                <Form onSubmit={addProductHandler} >
                <h1 className="my-5 text-center">Add Product</h1>
                <Form.Group controlId="courseName">
                    <Form.Label>Name:</Form.Label>
                    <Form.Control 
                        type="text" 
                        placeholder="Enter Name"
                        value={productName.value}
                        onChange={productNameInput}
                        onBlur={productNameInputBlur}
                        required
                    />
                </Form.Group>

                <Form.Group controlId="description">
                    <Form.Label>Description</Form.Label>
                    <Form.Control 
                        type="text" 
                        placeholder="Description"
                        value={productDescription.value}
                        onChange={productDescriptionInput}
                        onBlur={productDescriptionInputBlur}
                        required
                    />
                </Form.Group>

                <Form.Group controlId="price">
                    <Form.Label>Price</Form.Label>
                    <Form.Control 
                        type="number" 
                        placeholder="Price"
                        value={productPrice.value}
                        onChange={productPriceInput}
                        onBlur={productPriceInputBlur}
                        required
                    />
                </Form.Group>
        
            
                <Button variant="primary" type="submit" disabled={!isActive}>
                    Submit
                </Button>
                
                </Form>  
            </LoginRegisterFormBody>     }
        </>
    )
}


export async function addproductLoaderHandler({request,params}){


}